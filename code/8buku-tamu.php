<html>
<html lang="en">

<head>
    <title>Halaman Gabungan</title>
    <link rel="stylesheet" href="8assets/style.css">
</head>

<body>
    <?php include '8assets/header.html'; ?>
    <main>
        <h2>Konten Utama</h2>
        <p>Ini adalah konten utama dari halaman HTML Anda. Anda dapat menambahkan teks, gambar, dan elemen-elemen
            lainnya di sini.</p>
    </main>
    <?php include '8assets/footer.html'; ?>

</body>

</html>