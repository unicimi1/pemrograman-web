<html>

<head>
    <title>Profil Mahasiswa</title>
</head>

<body>
    <h1>Profil Mahasiswa</h1>
    <form method="post" action="hasildb.php">
        Nama: <input type="text" name="nama">
        <br><br>
        Umur: <input type="text" name="umur">
        <br><br>
        Alamat: <br>
        <textarea name="alamat" rows="4" cols="50"></textarea>
        <br><br>
        NIM: <input type="text" name="nim">
        <br><br>
        Jurusan:
        <select name="jurusan">
            <option value="TI">TI</option>
            <option value="SI">SI</option>
        </select>
        <br><br>
        Fakultas:
        <select name="fakultas">
            <option value="DKV dan TI">DKV dan TI</option>
            <option value="Psikologi">Psikologi</option>
            <option value="Manajemen">Manajemen</option>
        </select>
        <br><br>
        <input type="submit" value="Simpan">
    </form>

    <?php
    require 'config.php';
    $sql = "SELECT * FROM profil";
    $result = $conn->query($sql);

    // Memeriksa apakah query berhasil dijalankan
    if ($result->num_rows > 0) {
        // Menampilkan data baris per baris
        echo "<table border='1'>
        <tr>
        <th>Nama</th>
        <th>Umur</th>
        <th>Alamat</th>
        <th>NIM</th>
        <th>Jurusan</th>
        <th>Fakultas</th>
        </tr>";

        while ($row = $result->fetch_assoc()) {
            echo "<tr>
            <td>" . $row["nama"] . "</td>
            <td>" . $row["umur"] . "</td>
            <td>" . $row["alamat"] . "</td>
            <td>" . $row["nim"] . "</td>
            <td>" . $row["jurusan"] . "</td>
            <td>" . $row["fakultas"] . "</td>
            </tr>";
        }
        echo "</table>";
    } else {
        echo "Tidak ada data dalam tabel 'profil'";
    }

    // Menutup koneksi ke database
    $conn->close();
    ?>
</body>

</html>